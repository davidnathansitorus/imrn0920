// A. Tugas String

// Soal No. 1 (Membuat kalimat)
var word = "JavaScript";
var second = "is";
var third = "awesome";
var fourth = "and";
var fifth = "I";
var sixth = "love";
var seventh = "it!";

var combine =
  word +
  " " +
  second +
  " " +
  third +
  " " +
  fourth +
  " " +
  fifth +
  " " +
  sixth +
  " " +
  seventh;
console.log(combine);

// Soal No.2 Mengurai kalimat (Akses karakter dalam string)

var sentence = "I am going to be React Native Developer";
var exampleFirstWord = sentence[0];
var exampleSecondWord = sentence[2] + sentence[3];
var thirdWord =
  sentence[5] +
  sentence[6] +
  sentence[7] +
  sentence[8] +
  sentence[9] +
  sentence[10];
var fourthWord = sentence[11] + sentence[12] + sentence[13];
var fifthWord = sentence[14] + sentence[15] + sentence[16];
var sixthWord =
  sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
var seventhWord =
  sentence[23] +
  sentence[24] +
  sentence[25] +
  sentence[26] +
  sentence[27] +
  sentence[28] +
  sentence[29];
var eighthWord =
  sentence[30] +
  sentence[32] +
  sentence[33] +
  sentence[34] +
  sentence[35] +
  sentence[36] +
  sentence[37] +
  sentence[38];
console.log("\nFirst Word: " + exampleFirstWord);
console.log("Second Word: " + exampleSecondWord);
console.log("Third Word: " + thirdWord);
console.log("Fourth Word: " + fourthWord);
console.log("Fifth Word: " + fifthWord);
console.log("Sixth Word: " + sixthWord);
console.log("Seventh Word: " + seventhWord);
console.log("Eighth Word: " + eighthWord);

// Soal No. 3 Mengurai Kalimat (Substring)
var sentence2 = "wow JavaScript is so cool";

var exampleFirstWord2 = sentence2.substring(0, 3);
var secondWord2 = sentence2.substring(3, 14);
var thirdWord2 = sentence2.substring(14, 17);
var fourthWord2 = sentence2.substring(17, 20);
var fifthWord2 = sentence2.substring(20, 25);

console.log("\nFirst Word: " + exampleFirstWord2);
console.log("Second Word: " + secondWord2);
console.log("Third Word: " + thirdWord2);
console.log("Fourth Word: " + fourthWord2);
console.log("Fifth Word: " + fifthWord2);

// Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String
var sentence3 = "wow JavaScript is so cool";

var exampleFirstWord3 = sentence3.substring(0, 3);
var secondWord3 = sentence3.substring(4, 14);
var thirdWord3 = sentence3.substring(15, 17);
var fourthWord3 = sentence3.substring(18, 21);
var fifthWord3 = sentence3.substring(21, 25);

var firstWordLength = exampleFirstWord3.length;
var secondWordLength = secondWord3.length;
var thirdWordLength = thirdWord3.length;
var fourthWordLength = fourthWord3.length;
var fifthWordLength = fifthWord3.length;

// lanjutkan buat variable lagi di bawah ini
console.log(
  "\nFirst Word: " + exampleFirstWord3 + ", with length: " + firstWordLength
);
console.log(
  "Second Word: " + secondWord3 + ", with length: " + secondWordLength
);
console.log("Third Word: " + thirdWord3 + ", with length: " + thirdWordLength);
console.log(
  "Fourth Word: " + fourthWord3 + ", with length: " + fourthWordLength
);
console.log("Fourth Word: " + fifthWord3 + ", with length: " + fifthWordLength);

// B. Tugas Conditional
// if else
console.log("\n");
var nama = "Jane";
var peran = "Penyihir";

if (nama == "John" && peran == "") {
  console.log("Halo John, Pilih peranmu untuk memulai game!");
} else if (nama == "Jane" && peran == "Penyihir") {
  console.log("Selamat datang di Dunia Werewolf, Jane");
  console.log(
    "Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"
  );
} else if ((nama = "Junaedi" && peran == "werewolf")) {
  console.log("Selamat datang di Dunia Werewolf, Junaedi");
  console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
} else if (nama == "" && peran == "") {
  console.log("Nama harus diisi!");
} else if (nama == "Jenita" && peran == "Guard") {
  console.log("Selamat datang di Dunia Werewolf, Jenita");
  console.log(
    "Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."
  );
}

console.log("\n");
// Switch Case
var tanggal = 21;
var bulan = 1;
var tahun = 1945;

switch (bulan) {
  case 1: {
    console.log(tanggal, "Januari", tahun);
    break;
  }
  case 2: {
    console.log(tanggal, "Februari", tahun);
    break;
  }
  case 3: {
    console.log(tanggal, "Maret", tahun);
    break;
  }
  case 4: {
    console.log(tanggal, "April", tahun);
    break;
  }
  case 5: {
    console.log(tanggal, "Mei", tahun);
    break;
  }
  case 6: {
    console.log(tanggal, "Juni", tahun);
    break;
  }
  case 7: {
    console.log(tanggal, "Juli", tahun);
    break;
  }
  case 8: {
    console.log(tanggal, "Agustus", tahun);
    break;
  }
  case 9: {
    console.log(tanggal, "September", tahun);
    break;
  }
  case 10: {
    console.log(tanggal, "Oktober", tahun);
    break;
  }
  case 11: {
    console.log(tanggal, "November", tahun);
    break;
  }
  case 12: {
    console.log(tanggal, "Desember", tahun);
    break;
  }
  default: {
    console.log("bulan salah");
  }
}
