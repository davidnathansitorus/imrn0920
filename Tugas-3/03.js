// No. 1 Looping While
console.log("No. 1 Looping While");
console.log("LOOPING PERTAMA");
var x = 0;
while (x <= 20) {
  x = x + 2;
  console.log(x, " I love coding");
}
console.log("LOOPING KEDUA");
var y = 22;
while (y >= 2) {
  y = y - 2;
  console.log(y, "I will become a mobile developer");
}

// No. 2 Looping menggunakan for
console.log("\nNo. 2 Looping menggunakan for");
// SYARAT:
// A. Jika angka ganjil maka tampilkan Santai
// B. Jika angka genap maka tampilkan Berkualitas
// C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding.
var i = 0;
while (i < 20) {
  i++;
  if (i % 2 == 0) {
    console.log(i, "- Berkualitas");
  } else if (!(i % 2 == 0) && i % 3 == 0) {
    console.log(i, "- I Love Coding");
  } else {
    console.log(i, "- Santai");
  }
}
// No. 3 Membuat Persegi Panjang
console.log("\nNo. 3 Membuat Persegi Panjang");
for (var i = 0; i < 4; i++) {
  for (var j = 0; j < 8; j++) {
    process.stdout.write("#");
  }
  console.log(" ");
}
// No. 4 Membuat Tangga
console.log("\nNo. 4 Membuat Tangga");
for (var i = 0; i < 7; i++) {
  for (var j = 0; j <= i; j++) {
    process.stdout.write("#");
  }
  console.log(" ");
}
// No. 5 Membuat Papan Catur
console.log("\nNo. 5 Membuat Papan Catur");

var size = 8;
for (var i = 0; i < size; i++) {
  for (var j = 0; j < size; j++) {
    if ((i + j) % 2 == 0) {
      process.stdout.write(" ");
    } else {
      process.stdout.write("#");
    }
  }
  console.log(" ");
}
