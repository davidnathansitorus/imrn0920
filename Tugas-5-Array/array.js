// Soal No. 1 (Range)
console.log("Soal No. 1 (Range)");
function range(startNum, finishNum) {
  var x = [];
  if (startNum < finishNum) {
    for (var i = startNum; i <= finishNum; i++) {
      x.push(i);
    }
  } else if (startNum > finishNum) {
    for (var i = startNum; i >= finishNum; i--) {
      x.push(i);
    }
  } else if ((startNum == null && finishNum == null) || finishNum == null) {
    return -1;
  }
  return x;
}

console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11, 18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()); // -1

// Soal No. 2 (Range with Step)
console.log("\nSoal No. 2 (Range with Step)");
function rangeWithStep(startNum, finishNum, step) {
  var x = [];
  if (startNum < finishNum) {
    for (var i = startNum; i <= finishNum; i = i + step) {
      x.push(i);
    }
  } else if (startNum > finishNum) {
    for (var i = startNum; i >= finishNum; i = i - step) {
      x.push(i);
    }
  }
  return x;
}
console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)); // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)); // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)); // [29, 25, 21, 17, 13, 9, 5]

// Soal No. 3 (Sum of Range)
console.log("\nSoal No. 3 (Sum of Range)");
function sum(awal, akhir, step = 1) {
  if (awal < akhir) {
    for (total = 0; awal <= akhir; awal = awal + step) {
      total = total + awal;
      //   console.log(total - awal + " + " + awal + " = " + total);
    }
  } else if (awal > akhir) {
    for (total = 0; awal >= akhir; awal = awal - step) {
      total = total + awal;
    }
  }
  return total;
}
console.log(sum(1, 10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15, 10)); // 75
console.log(sum(20, 10, 2)); // 90

// Soal No. 4 (Array Multidimensi)
console.log("\nSoal No. 4 (Array Multidimensi)");
function dataHandling(x = [[]]) {
  for (var i = 0; i < x.length; i++) {
    var y = x[i];
    for (var j = 0; j < y.length; j++) {
      if (j == 0) {
        console.log("Nomor ID : " + y[j]);
      } else if (j == 1) {
        console.log("Nama Lengkap : " + y[j]);
      } else if (j == 2) {
        console.log("TTL : " + y[j]);
      } else if (j == 3) {
        console.log("Hobi : " + y[j] + "\n");
      }
      //   console.log("[" + i + "][" + j + "] = " + y[j]);
    }
  }
}

var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

dataHandling(input);

// Soal No. 5 (Balik Kata)
console.log("\nSoal No. 5 (Balik Kata)");
function balikKata(str) {
  var o = "";
  for (var i = str.length - 1; i >= 0; i--) {
    o = o + str[i];
  }
  return o;
}
console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I

// Soal No. 6 (Metode Array)
console.log("\nSoal No. 6 (Metode Array)");
/**
 *  keluaran yang diharapkan (pada console)
 
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989",
 * "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */

var input = [
  "0001",
  "Roman Alamsyah ",
  "Bandar Lampung",
  "21/05/1989",
  "Membaca",
];

function dataHandling2(input = []) {
  var l = input[1];
  input.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
  input.splice(4, 1, "Pria", "SMA Internasional Metro");

  var x = "";
  for (var i = 0; i < input.length; i++) {
    if (i == 3) {
      x = input[3];
    }
  }
  var y = x.split("/");

  var month = parseInt(y[1]);

  console.log(input);
  switch (month) {
    case 05: {
      console.log("mei");
      break;
    }
    default: {
      console.log("not a month");
      break;
    }
  }
  console.log(y);
  console.log(y[0] + "-" + y[1] + "-" + y[2]);
  console.log(l);
}

dataHandling2(input);
