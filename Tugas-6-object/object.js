// Soal No. 1 (Array to Object)
var p1 = [
  ["Abduh", "Muhamad", "male", 1992],
  ["Ahmad", "Taufik", "male", 1985],
];
var now = new Date();
var thisYear = now.getFullYear();

function arrayToObject(arr = [[]]) {
  // Code di sini

  var org1 = {
    firstName: arr[0][0],
    lastName: arr[0][1],
    gender: arr[0][2],
    age: thisYear - arr[0][3],
  };

  var org2 = {
    firstName: arr[1][0],
    lastName: arr[1][1],
    gender: arr[1][2],
    age: thisYear - arr[1][3],
  };

  if (arr[1][3] == null) {
    org2.age = "Invalid Birth Year";
  }
  console.log("1. " + org1.firstName + " " + org1.lastName + ":");
  console.log(org1);
  console.log("2. " + org2.firstName + " " + org2.lastName + ":");
  console.log(org2);
}

arrayToObject(p1);

// Driver Code
var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);

console.log("\n");

// Soal No. 2 (Shopping Time)

function shoppingTime(memberId = "", money = 0) {
  var shoping = {
    memberId: "0",
    money: 0,
    listPurchased: [],
    changeMoney: 0,
  };

  var memberdata = [
    "82Ku8Ma742",
    "1820RzKrnWn08",
    "234JdhweRxa53",
    "324193hDew2",
  ];

  if (memberId == "") {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } //
  else if (memberdata.includes(memberId)) {
    //
    if (money < 50000) {
      return "Mohon maaf, uang tidak cukup";
    } //
    else if (money >= 50000) {
      shoping.memberId = memberId;
      shoping.money = money;
      //
      if (money >= 1500000) {
        shoping.listPurchased.push("Sepatu Stacattu");
        var money_ = money - 1500000;
        shoping.changeMoney = money_;
        //
        if (money_ >= 500000) {
          shoping.listPurchased.push("Baju Zoro");
          var money_1 = money_ - 500000;
          shoping.changeMoney = money_1;
          //
          if (money_1 >= 250000) {
            shoping.listPurchased.push("Baju H&N");
            var money_2 = money_1 - 250000;
            shoping.changeMoney = money_2;
            //
            if (money_2 >= 175000) {
              shoping.listPurchased.push("Sweater Uniklooh");
              var money_3 = money_2 - 175000;
              shoping.changeMoney = money_3;
              //
              if (money_3 >= 50000) {
                shoping.listPurchased.push("Casing Handphone");
                shoping.changeMoney = money_3 - 50000;
              }
            }
          }
        }
        //
      } else if (money >= 500000) {
        shoping.listPurchased.push("Baju Zoro");
        var money1_ = money - 500000;
        shoping.changeMoney = money1_;
        //
        if (money1_ >= 250000) {
          shoping.listPurchased.push("Baju H&N");
          var money1_1 = money1_ - 250000;
          shoping.changeMoney = money1_1;
          //
          if (money1_1 >= 175000) {
            shoping.listPurchased.push("Sweater Uniklooh");
            var money1_2 = money1_1 - 175000;
            shoping.changeMoney = money1_2;
            //
            if (money1_2 >= 50000) {
              shoping.listPurchased.push("Casing Handphone");
              shoping.changeMoney = money1_2 - 50000;
            }
          }
        }
        //
      } else if (money >= 250000) {
        shoping.listPurchased.push("Baju H&N");
        var money2_ = money - 250000;
        shoping.changeMoney = money2_;
        //
        if (money2_ >= 175000) {
          shoping.listPurchased.push("Sweater Uniklooh");
          var money2_1 = money2_ - 175000;
          shoping.changeMoney = money2_1;
          //
          if (money2_1 >= 50000) {
            shoping.listPurchased.push("Casing Handphone");
            shoping.changeMoney = money2_1 - 50000;
          }
        }
        //
      } else if (money >= 175000) {
        shoping.listPurchased.push("Sweater Uniklooh");
        var money3_ = money - 175000;
        shoping.changeMoney = money3_;
        //
        if (money3_ >= 50000) {
          shoping.listPurchased.push("Casing Handphone");
          shoping.changeMoney = money3_ - 50000;
        }
      } //
      else if (money >= 50000) {
        shoping.listPurchased.push("Casing Handphone");
        var money4_ = money - 50000;
        shoping.changeMoney = money4_;
      }

      return shoping;
    }
  }
}

console.log(shoppingTime("1820RzKrnWn08", 2475000));
console.log(shoppingTime("82Ku8Ma742", 170000));
console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// Soal No. 3 (Naik Angkot)
function naikAngkot(arrPenumpang) {
  var rute = ["A", "B", "C", "D", "E", "F"];
  var arrOutput = [];
  if (arrPenumpang.length <= 0) {
    return [];
  }
  for (var i; i < arrPenumpang.length; i++) {
    var objOutput = {};
    var asal = arrPenumpang[i][1];
    var tujuan = arrPenumpang[i][2];

    var indexasal;
    var indexTujuan;

    for (var j = 0; j < rute.length; j++) {
      if (rute[j] == asal) {
        indexasal = j;
      } else if (rute[j] == tujuan) {
        indexTujuan = j;
      }
    }

    var bayar = (indexTujuan - indexasal) * 2000
    objOutput.penumpang = arrPenumpang[i][0];
    objOutput.naikdari = asal;
    objOutput.tujuan = tujuan;
    objOutput.bayar = bayar;

    arrOutput.push(objOutput);
  }
  return arrOutput;

}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]));

