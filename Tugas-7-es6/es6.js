// no.1
golden = () => {
    //isi function
    console.log("this is golden!!")
}
golden()

//no.2
const literal = (f, l) => {
    let studentName = {
        firstName: f,
        lastName: l
    };
    const { firstName, lastName } = studentName;
    console.log(firstName + " " + lastName);
}

literal("William", "Imoh");


//no.3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName: f, lastName: l, destination: d, occupation: o, spell: s } = newObject;

console.log(f, l, d, o)

//no.4
const umur = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const x = [...umur, ...east]
console.log(x)


// no.5
const planet = "earth"
const view = "glass"


var before = `Lorem ${view}dolor sit amet, 
consectetur adipiscing elit, ${planet}do eiusmod tempor incididunt 
ut labore et dolore magna aliqua. Ut enim  ad minim veniam`

// Driver Code
console.log(before)

