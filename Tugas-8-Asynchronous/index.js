// di index.js
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Tulis code untuk memanggil function readBooks di sini
readBooks(10000, books[0], function (check) {
    readBooks(7000, books[1], function (check) {
        readBooks(4000, books[2], function (check) { })
    })
})

// for (var i = 0; i < books.length; i++) {
//     console.log(books[i].name)
// }