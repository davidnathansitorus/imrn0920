class Animal {
    // Code class di sini
    constructor(name, legs = 4, cold_blooded = false) {
        this.name = name;
        this.legs = legs;
        this.cold_blooded = cold_blooded;

    }
}

class Ape extends Animal {
    constructor(name) {
        super(name);

    }
    yell() {
        console.log("Auooo");
    }

}

class Frog extends Animal {
    constructor(name) {
        super(name)
    }
    jump() {
        console.log("hop hop");
    }

}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false




// Code class Ape dan class Frog di sini

var sungokong = new Ape("Kera Sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

class Clock {
    constructor({ template }) {
        this.template = { template };
        this.timer;
    }
    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    }
    start() {
        render();
        this.timer = setInterval(render, 1000);
    }

}


var clock = new Clock({ template: 'h:m:s' });
clock.start();