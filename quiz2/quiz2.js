console.log("\nNO1")
class Score {
    // Code disini
    constructor(subject = "", points = [], email = "") {
        this.email = email;
        this.subject = subject;
        this.points = points;
    }

    average() {
        var total = 0;
        for (var i = 0; i < this.points.length; i++) {
            total += this.points[i];
        }
        return total / this.points.length;

    }
}


x = new Score("math", [100, 70, 90, 100, 85], "David@gmail.com");

console.log(x.average());



console.log("\nNO2")
const data = [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]

function viewScores(data = [], subject = "") {
    // code kamu di sini
    var arrOutput = [];

    for (var i = 1; i < data.length; i++) {

        var objOutput = {};



        if (data[0][1] == subject) {
            if (data[i][1] > 0) {
                objOutput.email = data[i][0];
                objOutput.subject = data[0][1];
                objOutput.points = data[i][1];
            }

        }
        if (data[0][2] == subject) {
            if (data[i][2] > 0) {
                objOutput.email = data[i][0];
                objOutput.subject = data[0][2];
                objOutput.points = data[i][2];
            }

        }
        if (data[0][3] == subject) {
            if (data[i][2] > 0) {
                objOutput.email = data[i][0];
                objOutput.subject = data[0][3];
                objOutput.points = data[i][3];
            }

        }

        arrOutput.push(objOutput);
    }
    console.log(arrOutput);
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")


console.log("\nNO3")
const data = [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]
function recapScores(data = []) {
    // code kamu di sini

    var sum1 = 0;
    for (var i1 = 1; i1 < data[1].length; i1++) {
        sum1 += data[1][i1];
    }
    var x1 = sum1 / (data[1].length - 1);



    var sum2 = 0;
    for (var i2 = 1; i2 < data[2].length; i2++) {
        sum2 += data[2][i2];
    }
    var x2 = sum2 / (data[2].length - 1);



    var sum3 = 0;
    for (var i3 = 1; i3 < data[3].length; i3++) {
        sum3 += data[3][i3];
    }
    var x3 = sum3 / (data[3].length - 1);



    var sum4 = 0;
    for (var i4 = 1; i4 < data[4].length; i4++) {
        sum4 += data[4][i4];
    }
    var x4 = sum4 / (data[4].length - 1);


    console.log(
        "1. email : " + data[1][0] + "\n" +
        "Rata-rata : " + Math.round(x1) + "\n" +
        "Predikat : " + "participant"

    )
    console.log(
        "2. email : " + data[2][0] + "\n" +
        "Rata-rata : " + Math.round(x2) + "\n" +
        "Predikat : " + "graduate"

    )
    console.log(
        "3. email : " + data[2][0] + "\n" +
        "Rata-rata : " + Math.round(x3) + "\n" +
        "Predikat : " + "graduate"

    )
    console.log(
        "4. email : " + data[2][0] + "\n" +
        "Rata-rata : " + Math.round(x4) + "\n" +
        "Predikat : " + "honour"

    )





}

recapScores(data);